package weatherapp;



public interface TemperatureHandler {

    // Convert response data into celcius
    public Boolean celsius();
    
    // Convert reponse data into fahrenheit
    public Boolean fahrenheit();
    
}