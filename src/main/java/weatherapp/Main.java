package weatherapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class Main {

    public static void main(String[] args) throws IOException {
        String input;
        try{
            TemperatureHandlerImpl tempratureHandle = new TemperatureHandlerImpl();
        System.out.println("tomorrow predicted temperature");
        System.out.println("Enter the 5 digit Zip code of the city in USA");
        BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
        
        while((input=br.readLine())!=null && !input.equals("exit") && !input.equals("quit")) {
            try {
                
                int zipcode = 75252;
                zipcode = Integer.parseInt(input);
                tempratureHandle.getCoolestTempratureOfCity(zipcode);
                
            } catch (NumberFormatException e) {
                System.out.println("Please enter a valid zip code\n");
            }
            
            System.out.println("Want to check temperature at another location,\nthen please Enter the 5 digit Zip code of the city in USA");
            
        }
        System.out.println(".....Program Exited.....");
        
        }catch (IOException e) {
            
            e.printStackTrace();
        }
        
    }
}    