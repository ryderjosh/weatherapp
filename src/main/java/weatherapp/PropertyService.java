package weatherapp;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService {

public String getProperty(String propertyName) {
        
        try (InputStream input = new FileInputStream("config.properties")) { 
            Properties prop = new Properties();

            // load a properties file
            prop.load(input);
            return prop.getProperty(propertyName);
        }
        catch(FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch(IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // Default temprature
        return "C";
    }
}