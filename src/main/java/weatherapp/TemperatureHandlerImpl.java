package weatherapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TemperatureHandlerImpl implements TemperatureHandler{

    // Creating object for the default configuration
    PropertyService propertyService = new PropertyService();
    char defaultTempNotation;
    
    @Override
    public Boolean celsius() {
        
        return true;
    }

    @Override
    public Boolean fahrenheit() {
        
        return false;
    }
    /**
     * Here you will get default temprature from the setting configuration
     * @return String bases on the default configuration
     */
    public String defaultTemprature() {
        
        if(propertyService.getProperty("app.temprature").equals("C")) {
            defaultTempNotation = 'C';
            return "true";
        }
        defaultTempNotation = 'F';
        return "false";
    }
    
    /**
     * Get temprature based on the zipcode
     * @param zipcode : zipcode of the united states
     */
    public void getCoolestTempratureOfCity(Integer zipcode) {
        try {
            PropertyService propertyService = new PropertyService();
            URL url = null;
            
            if(propertyService != null) {
                url = new URL(propertyService.getProperty("app.url")+
                    "?app_id="+propertyService.getProperty("app.id")+
                    "&app_code="+propertyService.getProperty("app.code")+
                    "&product=forecast_hourly&metric="+defaultTemprature()+
                    "&zipcode=" + zipcode);

                url.openStream().close();
                // Get the input stream through URL Connection
                URLConnection con = url.openConnection();
                
                InputStream is = con.getInputStream();

                BufferedReader ar = new BufferedReader(new InputStreamReader(is));

                String line = null;
                    
                line = ar.readLine();
                
                JSONObject obj = null;
                obj = new JSONObject(line);
                
                JSONArray arr = null;
                arr = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getJSONArray("forecast");
                String cityName = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getString("city");
                
                ArrayList<Double>  temp = new ArrayList<Double>();
                ArrayList<String> dateTime = new ArrayList<String>();
                int i = 0;
                String today = arr.getJSONObject(i).getString("weekday");
                for (i=1;today.equals(arr.getJSONObject(i).getString("weekday"));i++)
                {}
                for (int j=0; j < 24; i++,j++)
                {
                    temp.add(Double.parseDouble((arr.getJSONObject(i).getString("temperature"))));
                    dateTime.add(arr.getJSONObject(i).getString("localTime"));
                }
                int min_index = temp.indexOf(Collections.min(temp));
                String time = dateTime.get(min_index).substring(0,2);
                String date = dateTime.get(min_index).substring(2, dateTime.get(min_index).length());
                date = date.substring(0,2) + "-" + date.substring(2,4) + "-" + date.substring(4,8);
                String meridiem = "am";
                //Display time in meridiem
                if(Integer.parseInt(time) > 11)
                {
                    if(time!="12")
                        time = Integer.toString((Integer.parseInt(time) - 12));
                    meridiem = "pm";
                } else {
                    if(time=="00")
                        time = "12";
                }
                //Finds the coolest hour of tomorrow for the city of user feeded zipcode
                System.out.println("coolest hour in " + cityName + " tomorrow on " + date + " would be at " + time + " " + meridiem);
                System.out.println("and the temperature would be " + temp.get(min_index) + " "+ defaultTempNotation + " \n");
            }
            
        }catch (IOException e) {
            System.out.println("City not found, please enter a valid Zip Code\n");
            return;
        }catch (JSONException e) {
            System.out.println(" Invalid API format ");
        }

    }
}